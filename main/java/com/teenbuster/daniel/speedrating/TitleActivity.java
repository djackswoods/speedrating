package com.teenbuster.daniel.speedrating;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;


public class TitleActivity extends ActionBarActivity implements View.OnClickListener{

    public Button startbtn,readmebtn,aboutbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);

        startbtn = (Button)findViewById(R.id.startbtn);
        startbtn.setOnClickListener(this);
        readmebtn = (Button)findViewById(R.id.ReadMeBtn);
        readmebtn.setOnClickListener(this);
        aboutbtn = (Button)findViewById(R.id.aboutBtn);
        aboutbtn.setOnClickListener(this);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_title, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void aboutbtnClick(){
        startActivity(new Intent(".SpeedRating.AboutCreator"));
    }

    private void readmebtnClick(){
        startActivity(new Intent(".SpeedRating.ReadMe"));
    }

    private void startbtnClick(){

        startActivity(new Intent(".SpeedRating.MainActivity"));
    }



    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.startbtn:
                startbtnClick();
                break;
            case R.id.ReadMeBtn:
                readmebtnClick();
                break;
            case R.id.aboutBtn:
                aboutbtnClick();
                break;
        }
    }
}
