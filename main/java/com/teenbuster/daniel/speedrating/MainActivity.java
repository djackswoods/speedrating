package com.teenbuster.daniel.speedrating;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.NumberFormat;


public class MainActivity extends ActionBarActivity implements LocationListener, View.OnClickListener{


    public Button compass_btn;
    //public Location loc;
//    double Slat = loc.getLatitude();
//    double Slong = loc.getLongitude();
//    float distTraveled;
//    float results[];

    NumberFormat speedFormatter = NumberFormat.getNumberInstance();
    //NumberFormat distanceFormatter = NumberFormat.getNumberInstance();
    NumberFormat latFormatter = NumberFormat.getNumberInstance();
    NumberFormat longFormatter = NumberFormat.getNumberInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Creates the new Location manager to be used by the GPS in the device
        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        //Sets the update time for when to grab the new GPS location from the provided GPS
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        /*calls the Main functioning method which holds all of the calculations that are run
          whenever the location changes
        */
        this.onLocationChanged(null);
        /*
            Creates the button and the on click listener to link to the next activity
         */
        compass_btn = (Button)findViewById(R.id.compass);
        compass_btn.setOnClickListener(this);



    }
    /*
        Creats the onclick method to link to the Compass Activities intent in order to
        switch to the next Activity
     */
    private void compassBtnClick(){

        startActivity(new Intent(".SpeedRating.CompassActivity"));
    }

    /*
        Generates the switch case for when the button is clicked and calls the compassBtnClick()
        and switches to the new activity
     */
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.compass:
                compassBtnClick();
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(Location location) {
        float nCurrentSpeed;
        //double Elat;
        //double Elong;



        TextView dist =(TextView)this.findViewById(R.id.distanceView);
        TextView speed = (TextView)this.findViewById(R.id.speedView);

        TextView lat =(TextView)this.findViewById(R.id.CurrLat);
        TextView cLong =(TextView)this.findViewById(R.id.CurrLong);

        if (location == null){
            //sets speed to -.- when there is no speed
            speed.setText("-.- mph");
            lat.setText("-.-");
            cLong.setText("-.-");
            dist.setText("-.-");
            //when there is no speed, pick up and assigns the current lat and long of the location
           // nCurrentLatitude = location.getLatitude();
           // nCurrentLong = location.getLongitude();
        }
        else {

            //speed.setText("0.0 mph");
            dist.setText("0.0 miles");
//            Elat = location.getLatitude();
//            Elong = location.getLongitude();
//
//            Location.distanceBetween(Slat, Slong, Elat, Elong, results);
//
//            for (float result : results) {
//                distTraveled = distTraveled + result;
//            }
//            distanceFormatter.setMinimumFractionDigits(2);
//            distanceFormatter.setMaximumFractionDigits(2);
//            String distOut = distanceFormatter.format(distTraveled);
//            dist.setText(distOut);

            /*
                This is the calculation for the speedometer. It takes in the speed given from
                the location.getSpeed() which is in meters per second, then converts it to miles
                per second. After it calculates the speed, uses the Formatter to convert it from
                a float to a string, and limits the decimal places to 2 places
            */
            nCurrentSpeed = location.getSpeed();
            nCurrentSpeed *= 2.23694;
            speedFormatter.setMinimumFractionDigits(2);
            speedFormatter.setMaximumFractionDigits(2);
            String output = speedFormatter.format(nCurrentSpeed);
            speed.setText(output + " mph");

            /*
                Uses the location.getLatitude() and location.GetLongitude(), then converts the double
                to a string and stores it into the proper text view. Limits the decimal places to 2
                places in order to cut down on the size of the text
             */
            longFormatter.setMinimumFractionDigits(2);
            latFormatter.setMinimumFractionDigits(2);
            latFormatter.setMaximumFractionDigits(2);
            longFormatter.setMaximumFractionDigits(2);
            String latOut = latFormatter.format(location.getLatitude());
            String longOut = longFormatter.format(location.getLongitude());
            cLong.setText(longOut);
            lat.setText(latOut);
            }
        }



    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
